<?php
Class Graph_model
{
    function __construct(){
            try
        {
            $this->conn= Conexion::Conectar('Votante','usbw');
        }
        catch(Exception $e)
        {
            die('Error: '.$e->GetMessage());
            echo " Linea de error: ". $e->getLine();
        }
    }
    //Funcion que devuelve el array con los votos
    public function graph_datos(){
        $papeleta_sql="select NombrePartido,Votos from Resultados;";
        $papeleta_resultado = $this->conn->prepare($papeleta_sql);
        $papeleta_resultado->execute();
        return $papeleta_resultado->fetchAll(PDO::FETCH_OBJ);
    }
}