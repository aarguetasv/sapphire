<?php
	
	include_once 'autoload.php';

	Class Model_administrador
	{
		private $conn;
	
		public function __construct(){
			try
			{
				$this->conn= Conexion::Conectar('Admin1','usbw');
			}
			catch(Exception $e)
			{
				die('Error: '.$e->GetMessage());
				echo " Linea de error: ". $e->getLine();
			}
		}
		function Admin_login($user,$pw)
		{
			$sql = "SELECT Usuario FROM Admin WHERE Usuario=? AND Contrasena = AES_ENCRYPT(?,'sapphire');";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($user,$pw));
			$datos = $resultado->fetch(PDO::FETCH_ASSOC);

			if ($datos) {
				session_start();
				$_SESSION['Usuario'] = $datos['Usuario'];
				header ('location:indexAdministrador?controller=administrador&accion=home');
			}else
			{
				header('location:indexAdministrador');
			}
		}
		function Admin_logout()
		{
			session_start();
 	    	unset($_SESSION['Usuario']);
        	header ('location:indexAdministrador');
		}
	}	