<?php
	
	include_once 'autoload.php';

	Class Model_partidos
	{
		private $conn;
		public $IdPartido;
		public $NombrePartido;
		public $Imagen;

		public function __construct(){
			try
			{
				$this->conn= Conexion::Conectar('Admin1','usbw');
			}
			catch(Exception $e)
			{
				die('Error: '.$e->GetMessage());
				echo " Linea de error: ". $e->getLine();
			}
		}

		function Listar()
		{
			$sql = "SELECT IdPartido,NombrePartido, Imagen FROM Partidos";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute();
			return $resultado->fetchAll(PDO::FETCH_OBJ);
		}
		function getById($id)
		{
			$sql = "SELECT IdPartido, NombrePartido, Imagen FROM Partidos where IdPartido = ?";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($id));
			return $resultado->fetch(PDO::FETCH_OBJ);
		}
		function Actualizar($partido)
		{
			$sql =  "UPDATE Partidos SET NombrePartido = ?, Imagen = ? WHERE IdPartido  = ?";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($partido->NombrePartido, $partido->Imagen, $partido->IdPartido));
		}
		function Agregar($partido)
		{
			$sql = 'INSERT INTO Partidos(NombrePartido,Imagen) VALUES(?, ?)';
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($partido->NombrePartido,
				$partido->Imagen
			));
		}
		function Eliminar($partido)
		{
			$sql = "DELETE FROM Partidos WHERE IdPartido = ?";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($partido->IdPartido));
		}
	}