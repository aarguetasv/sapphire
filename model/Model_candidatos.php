<?php
	
	include_once 'autoload.php';

	Class Model_candidatos
	{
		private $conn;
		public $IdCandidato;
		public $Nombre;
		public $Apellido;
		public $IdMunicipio;
		public $IdPartido;
		public $Imagen;

		public function __construct(){
			try
			{
				$this->conn= Conexion::Conectar('Admin1','usbw');
			}
			catch(Exception $e)
			{
				die('Error: '.$e->GetMessage());
				echo " Linea de error: ". $e->getLine();
			}
		}

		function Listar()
		{
			$sql = "SELECT C.IdCandidato, C.Nombre, C.Apellido, M.NombreMunicipio, P.NombrePartido, C.Imagen FROM ((Candidatos C INNER JOIN Municipios M ON C.idMunicipio= M.IdMunicipio) INNER JOIN Partidos P ON C.idPartido = P.IdPartido);";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute();
			return $resultado->fetchAll(PDO::FETCH_OBJ);
		}
		//used in datalist
		function ListarMunicipios()
		{
			$sql = "SELECT IdMunicipio,NombreMunicipio FROM Municipios";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute();
			return $resultado->fetchAll(PDO::FETCH_OBJ);
		}
		function getById($id)
		{
			$sql = "SELECT IdCandidato, Nombre, Apellido, IdMunicipio, IdPartido, Imagen FROM Candidatos where IdCandidato = ?";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($id));
			return $resultado->fetch(PDO::FETCH_OBJ);
		}
		function Actualizar($candidato)
		{
			$sql =  "UPDATE Candidatos SET Nombre = ?, Apellido = ?, IdMunicipio = ?, IdPartido = ?, Imagen = ? WHERE IdCandidato  = ?";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($candidato->Nombre,
				$candidato->Apellido,
				$candidato->IdMunicipio,
				$candidato->IdPartido,
				$candidato->Imagen,
				$candidato->IdCandidato));
		}
		function Agregar($candidato)
		{
			$sql = 'INSERT INTO Candidatos(Nombre,Apellido,IdMunicipio,IdPartido, Imagen) VALUES(?, ?, ?, ?, ?)';
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($candidato->Nombre,
				$candidato->Apellido,
				$candidato->IdMunicipio,
				$candidato->IdPartido,
				$candidato->Imagen
			));
		}
		function Eliminar($candidato)
		{
			$sql = "DELETE FROM Candidatos WHERE IdCandidato = ?";
			$resultado = $this->conn->prepare($sql);
			$resultado->execute(array($candidato->IdCandidato));
		}
	}