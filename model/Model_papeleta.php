<?php

include_once('autoload.php');

Class Model_papeleta{
	
	private $conn;
	
	public function __construct(){
		try
		{
			$this->conn= Conexion::Conectar('Votante','usbw');
		}
		catch(Exception $e)
		{
			die('Error: '.$e->GetMessage());
			echo " Linea de error: ". $e->getLine();
		}
	}
	
	public function tomar_background(){
		$sql="select C.IdCandidato, C.IdPartido, P.Imagen from Candidatos C inner join Partidos P on C.IdPartido = P.IdPartido where C.IdCandidato != 1";
		$resultado = $this->conn->prepare($sql);
		$resultado->execute();
		return $resultado->fetchAll(PDO::FETCH_OBJ);
	}
	public function Votar($voto,$dui)
	{
		$sql = "CALL procedimientoVoto(?,?)";
		$resultado = $this->conn->prepare($sql);
		$resultado->execute(array($dui,$voto));
		echo " $dui $voto";
	}
}

?>