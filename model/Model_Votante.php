<?php

Class Model_Votante{
	private $conn;
	
	function __construct(){
			try
		{
			$this->conn= Conexion::Conectar('Votante','usbw');
		}
		catch(Exception $e)
		{
			die('Error: '.$e->GetMessage());
			echo " Linea de error: ". $e->getLine();
		}
	}
	function Votante_login($input_DUIS,$input_PW)
	{	
		try{
			$sql_votante="call infoPersona(:input_DUI,:input_PW);";
			$resultado_votante = $this->conn->prepare($sql_votante);
			$resultado_votante->execute(array('input_DUI'=>$input_DUIS, 'input_PW'=>$input_PW));
			$datos_votante= $resultado_votante->fetch(PDO::FETCH_ASSOC);
		}finally{
			
			if($datos_votante==False){
				/*echo "<h1>DUI $input_DUI no encontrado</h1>";
				$var[] = var_dump($datos_votante);
				$var[]= $input_DUIS;
				$var[] =  $votante->DUI;
				return var_dump($var);*/
				require_once 'view/login/login.php';
				echo "<script>alert('Datos incorrectos'); </script>";
				//return false;
				
			} 
			else {
				var_dump($datos_votante);
				echo "<font color=red>".$datos_votante["Nombre"]."</font>";
				Session_Start();
				$_SESSION['DUI'] = $datos_votante["DUI"];
				$_SESSION['Nombre'] = $datos_votante["Nombre"];
				$_SESSION['Apellido'] = $datos_votante["Apellido"];
				$_SESSION['Municipio'] = $datos_votante["NombreMunicipio"];
				$_SESSION['Departamento'] = $datos_votante["NombreDepto"];
				$_SESSION['Imagen'] = $datos_votante["Imagen"];
				$_SESSION['VotoRealizado'] = $datos_votante["VotoRealizado"];
					
				//echo "ye,logeado<br>";	
				//var_dump ($_SESSION);
				header ("location:index?controller=sapphire&accion=ver_papeleta");
			}
		}
	}
	public function Votante_Logout(){
		session_start();
 	    unset($_SESSION['Nombre']);
        header ('location:index');
	}
}


?>
