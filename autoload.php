<?php

spl_autoload_register(function($clase)
        {
            if(file_exists($clase . ".php"))
            {
            	include_once $clase . ".php";
            }
            elseif(file_exists("controller/" . $clase . ".php"))
            {
            	include_once "controller/" . $clase . ".php";
            }
            elseif(file_exists("model/".$clase.".php"))
            {
            	include_once "model/" . $clase . ".php";
            }
            
        });