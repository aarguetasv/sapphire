<?php
Session_start();
if(!isset($_SESSION["Usuario"])){
    header ('location:indexAdministrador');
}
   	include_once('view/administrador/admin_header.php');
?>
<section class="feature-area section-gap">
	<a href="?controller=Administrador&accion=Candidatos"><button class="btn btn-default" style="margin-bottom:15vh;margin-left:30vh;width:300px;height:300px;background-color:#0151aa;color:white;font-size: 40px" >CANDIDATOS</button></a>
	<a href="?controller=Administrador&accion=Partidos"><button class="btn btn-default" style="margin-right:30vh;margin-bottom:15vh;margin-left:40vh;width:300px;height:300px;background-color:#0151aa;color:white;font-size: 40px" >PARTIDOS</button></a>
</section>

<?php
	//Llamada al footer
	include('view/administrador/admin_footer.php');
//}
?>