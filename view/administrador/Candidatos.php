<?php
Session_start();
if(!isset($_SESSION["Usuario"])){
    header ('location:indexAdministrador');
}
	//Llamada al header
	include_once('view/administrador/admin_header.php');
?>
<section class="feature-area section-gap">
	<h1 style="text-align: center;">CANDIDATOS</h1>	<br>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Municipio</th>
				<th>Partido</th>
				<th>Imagen</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->Candidatos->Listar() as $key) :  ?>
			<tr>
				<td><?php echo $key->IdCandidato; ?></td>
				<td><?php echo $key->Nombre; ?></td>
				<td><?php echo $key->Apellido; ?></td>
				<td><?php echo $key->NombreMunicipio; ?></td>
				<td><?php echo $key->NombrePartido; ?></td>
				<td><?php echo $key->Imagen; ?></td>
				<td><a href="?controller=Administrador&accion=EditarCandidato&IdCandidato= <?php echo $key->IdCandidato; ?>">Editar</a></td>
				<td><a href="?controller=Administrador&accion=Eliminar&IdCandidato= <?php echo $key->IdCandidato; ?>">Eliminar</a></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table><br>
	<center>
	<a href="?controller=Administrador&accion=EditarCandidato"><button class="btn btn-default" style="width:200px;height:50px;background-color:#70a94e;color:white;">Agregar Candidato</button></a>
</center><br>
</section>
<?php
	//Llamada al footer
	include('view/administrador/admin_footer.php');
?>