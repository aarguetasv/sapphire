<?php
Session_start();
if(!isset($_SESSION["Usuario"])){
    header ('location:indexAdministrador.php');
}
	//Llamada al header
	include_once('view/administrador/admin_header.php');
?>
<section class="feature-area section-gap">
	<h1 style="text-align: center;">PARTIDOS</h1>	<br>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Imagen</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($this->partidos->Listar() as $key) :  ?>
			<tr>
				<td><?php echo $key->IdPartido; ?></td>
				<td><?php echo $key->NombrePartido; ?></td>
				<td><?php echo $key->Imagen; ?></td>
				<td><a href="?controller=Administrador&accion=EditarPartido&IdPartido= <?php echo $key->IdPartido; ?>">Editar</a></td>
				<td><a href="?controller=Administrador&accion=Eliminar&IdPartido= <?php echo $key->IdPartido; ?>">Eliminar</a></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table><br>
	<center>
	<a href="?controller=Administrador&accion=EditarPartido"><button class="btn btn-default"style="width:200px;height:50px;background-color:#70a94e;color:white;">Agregar Partido</button></a>
	</center><br>
</section>
<?php
	//Llamada al footer
	include('view/administrador/admin_footer.php');
?>