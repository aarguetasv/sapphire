<!-- INICIA EL AREA DEL FOOTER -->
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-4  col-md-6">
							<div class="single-footer-widget mail-chimp">

									<img src="assets/img/escudo.png" class="escudo">
								<p class="footer-text">
									Otra vez el escudo
								</p>
								<h3>012-6532-568-9746</h3>
								<h3>012-6532-568-97468</h3>
							</div>
						</div>
						
						<div class="col-lg-4  col-md-6">
							<div class="single-footer-widget mail-chimp">
								<img src="assets/img/loguito.png">
								<h3>Mas informacion</h3>
								<h3>mas informacion del footer</h3>
							</div>
						</div>
					</div>
					<div class="col-lg-4  col-md-6">
							<div class="single-footer-widget mail-chimp">
								<?php if (isset($_SESSION['Usuario'])) {
									echo "<a href='?controller=administrador&accion=Logout'><button class='btn btn-default'>Salir<span class='lnr lnr-arrow-right' style='padding-left:10px'></span></button></a>";
									} ?>
							</div>
						</div>
					</div>
					

				
				</div>
			</footer>
			<!-- TERMINA EL AREA DEL FOOTER -->

			<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
			<script src="assets/js/vendor/bootstrap.min.js"></script>
			<script src="assets/js/jquery.ajaxchimp.min.js"></script>
			<script src="assets/js/jquery.nice-select.min.js"></script>
			<script src="assets/js/jquery.sticky.js"></script>
			<script src="assets/js/parallax.min.js"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
			<script src="assets/js/jquery.magnific-popup.min.js"></script>
			<script src="assets/js/waypoints.min.js"></script>
			<script src="assets/js/jquery.counterup.min.js"></script>
			<script src="assets/js/main.js"></script>
		</body>
</html>