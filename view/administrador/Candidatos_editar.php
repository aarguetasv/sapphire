<?php
Session_start();
if(!isset($_SESSION["Usuario"])){
    header ('location:indexAdministrador');
}
	//Llamada al header
	include_once('view/administrador/admin_header.php');
?>
<section class="feature-area section-gap">

	<form action=?controller=Administrador&accion=Agregar method="POST">
		<input type="hidden" name="IdCandidato" value="<?php echo $candi->IdCandidato; ?>">
		<label>Nombre</label>
		<input type="text" name="Nombre" value="<?php echo $candi->Nombre; ?>"><br>
		<label>Apellido</label>
		<input type="text" name="Apellido" value="<?php echo $candi->Apellido; ?>"><br>
		<label>Municipio</label>
		<input type="text" name="IdMunicipio" list="Municipios" value="<?php echo $candi->IdMunicipio; ?>"><br>
		<datalist id="Municipios">
			<?php foreach($this->Candidatos->ListarMunicipios() as $m) : ?>
			<option value="<?php echo $m->IdMunicipio; ?>"><?php echo $m->NombreMunicipio ?></option>
			<?php endforeach; ?>
		</datalist><br>
		<label>Partido</label><br>
		<select name="IdPartido">
			<option value="<?php echo $candi->IdPartido; ?>" selected>Seleccione el Partido</option>
			<?php foreach($this->partidos->Listar() as $c): ?>
	        <option value="<?php echo $c->IdPartido; ?>"><?php echo $c->NombrePartido; ?></option>
	        <?php endforeach; ?>
		</select><br>
		<!--<input type="text" name="IdPartido" value="<?php echo $candi->IdPartido; ?>"><br>-->
		<br><label>Imagen</label>
		<input type="text" name="Imagen" value="<?php echo $candi->Imagen; ?>"><br>
		<center>
		<br><button class="btn btn-default" style="width:200px;height:50px;background-color:#70a94e;color:white;">Guardar</button>
		</center>
	</form>

</section>

<?php
	//Llamada al footer
	include('view/administrador/admin_footer.php');
?>