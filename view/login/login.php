<?php
	include_once('view/login/login_header.php');
?>

<!-- Start blog Area -->
<section class="blog-area section-gap">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8 pb-30 header-text">
				<h1>Ingresar</h1>
				<p>
					Escriba su DUI y Contraseña para continuar
				</p>
				<center>			
					<div id="mc_embed_signup">
						<form action="?controller=sapphire&accion=login" method="POST" class="form-inline">
								
							<div class="form-group row" style="width: 100%">
								<div class="col-lg-8 col-md-12">
									<center>DUI.</center>
									<input name="input_DUI" placeholder="xxxxxxx-x" type="text" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.keyCode == 8' maxlength='9'>
									<?php
									?>
								</div>
									
								<div class="col-lg-8 col-md-12">
									<center>Contraseña.</center>
									<input name="input_pw" placeholder="Contraseña" type="password" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.keyCode == 8' maxlength='9'>
								</div>
									
										
								<div class="col-lg-4 col-md-12">
									<input type="submit" name="votante_enviar" Value="Entrar" class="nw-btn primary-btn">
								</div>
							</div>
						</form>
					</div>
				</center>
			</div>
		</div>
	</div>
</section>
			<!-- end blog Area -->

<?php
	include_once('view/login/login_footer.php');
?>
