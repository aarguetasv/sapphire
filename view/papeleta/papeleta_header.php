<?php
Session_start();
if(!isset($_SESSION["Nombre"])){
    header ('location:index?controller=sapphire&accion=Index');
}
?> 		<body>
			<!-- Inicia el area de header -->
			<header class="default-headeruser">
				<div class="container">
					<div class="header-wrap">
						<div class="header-top d-flex justify-content-between align-items-center">
							<div class="logo">
								<a href="#home"><img src="assets/img/logo.png" alt=""></a>
							</div>
							<div class="main-menubar d-flex align-items-center">
								<div class="single-feature d-flex flex-row pb-30" style="margin-top:30px">
									<div class="desc">
										<p class="p_arriba">
											<?php
												echo $_SESSION["Nombre"]." ".$_SESSION['Apellido'];
											?>
										</p>
										<p class="p_arriba">
											<?php
												echo $_SESSION["Municipio"] . ", " . $_SESSION["Departamento"];
											?>
										</p>
										<p class="p_arriba">
											<?php
												echo $_SESSION['DUI'];
											?>
										</p>
									<a href='?controller=sapphire&accion=Logout'><button class="btn btn-default">No soy yo, salir<span class="lnr lnr-arrow-right" style="padding-left:10px"></span></button></a>
									</div>
									<div class="icon">
										<img src="assets/img/<?php echo $_SESSION['Imagen'];?>" class="escudo">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!-- Finaliza el area de header -->