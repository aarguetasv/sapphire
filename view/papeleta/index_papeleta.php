
<?php
	//Llamada al header

	include_once('view/papeleta/papeleta_header.php');
?>


<!-- Inicia la seccion principal de la página -->
<section class="feature-area section-gap" id="service">
	<form action="index.php?controller=sapphire&accion=Votar" method="POST">
		<div class="container" style="padding-top:2vh; padding-left:10vh;">
			<div class="row">
	<!-- INICIO DEL FOR PARA REPETIR LOS CUADRADOS -->
			
				<?php 
					$papeleta_img ="background: url(assets/img/";
					foreach($this->Papeleta_controller->tomar_background() as $datos):
				?>
				<label class="container2" style="<?php echo $papeleta_img . $datos->Imagen.")"?>">
					<input type="checkbox" name="<?php echo $datos->IdPartido ?>">
					<span class="checkmark"></span>
				</label>

				<?php 
					endforeach; 
				?>
	<!--FIN DEL CICLO FOR PARA LOS CUADRADOS -->							
			</div>
				<center>	
					<button class="btn btn-default" style="margin-right:15vh;margin-bottom:15vh;widht:100px;height:50px;background-color:#70a94e;color:white;" <?php if($_SESSION['VotoRealizado'] == 1){echo "disabled";} ?>>Terminar de votar</button>
				</center>
			
		</div>
	</form>
</section>
<!-- Termina la seccion principal de la página -->

<?php
	//Llamada al footer
	include('view/papeleta/papeleta_footer.php');
?>