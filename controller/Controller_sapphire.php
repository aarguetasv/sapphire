<?php

require_once('autoload.php');


class Controller_sapphire
{
		private $Papeleta_controller;
		private $Votante_controller;
		private $Graph_controller;
		
		function __construct()
		{
			$this->Papeleta_controller= new Model_papeleta();
			$this->Votante_controller= new Model_Votante();
			$this->Graph_controller= new Graph_model();
		}
		function Index()
		{
			require_once 'view/login/login.php';
		}
		function ver_papeleta()
		{
			require_once 'view/papeleta/index_papeleta.php';
		}
		public function login(){
			
			$input_DUI=$_POST['input_DUI'];
			$input_Pass=$_POST['input_pw'];

			$this->Votante_controller->Votante_login($input_DUI,$input_Pass);
		}
		public function Logout()
		{
        	$this->Votante_controller->Votante_Logout();
		}
		public function Votar()
		{
			$marcas;
			
			foreach ($this->Papeleta_controller->tomar_background() as $key) {
				if (isset($_POST[$key->IdPartido]) && $_POST[$key->IdPartido] == 'on') {
					$marcas[] = $key->IdCandidato;
				}
			}

			$voto = (count($marcas) != 1) ? 1 : $marcas[0];
			
			session_start();
			$this->Papeleta_controller->Votar($voto,$_SESSION['DUI']);
			$this->Votante_controller->Votante_Logout();
		}
		function Grafica()
		{
			include_once('view/grafica/grafica_index.php');
		}
}

