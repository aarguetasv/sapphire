<?php

	require 'autoload.php';

	Class Controller_administrador
	{
		private $adminstrador;
		private $Candidatos;
		private $partidos;

		function __construct()
		{
			$this->administrador = new Model_administrador();
			$this->Candidatos = new Model_candidatos();
			$this->partidos = new Model_partidos();
		}

		function Index()
		{
			require_once 'view/administrador/login.php';
		}
		function login()
		{
			//from view/administrador/login
			$input_usuario = $_REQUEST['input_usuario'];
			$input_contrasena = $_REQUEST['input_contrasena'];
			$this->administrador->Admin_login($input_usuario,$input_contrasena);
		}
		function logout()
		{
			$this->administrador->Admin_logout();
		}
		function home()
		{
			require_once 'view/administrador/administrador.php';
		}
		function Candidatos()
		{
			require_once 'view/administrador/Candidatos.php';
		}
		function Partidos()
		{
			require_once 'view/administrador/Partidos.php';
		}
		function EditarCandidato()
		{
			$candi = new Model_Candidatos();

			if (isset($_REQUEST['IdCandidato'])) {	
	
				$candi = $this->Candidatos->getById($_REQUEST['IdCandidato']);
			}
				require_once 'view/administrador/Candidatos_editar.php';
		}
		function EditarPartido()
		{
			$part = new Model_partidos();
			
			if (isset($_REQUEST['IdPartido'])) {
				
				$part = $this->partidos->getById($_REQUEST['IdPartido']);

			}
				require_once 'view/administrador/Partidos_editar.php';
			
		}
		function Agregar()
		{
			if (isset($_REQUEST['IdCandidato'])){
				$candi = new Model_Candidatos();

				$candi->IdCandidato = $_REQUEST['IdCandidato'];
				$candi->Nombre = $_REQUEST['Nombre'];
				$candi->Apellido = $_REQUEST['Apellido'];
				$candi->IdMunicipio = $_REQUEST['IdMunicipio'];
				$candi->IdPartido = $_REQUEST['IdPartido'];
				$candi->Imagen = $_REQUEST['Imagen'];

				$candi->IdCandidato >0 ? $this->Candidatos->Actualizar($candi) : $this->Candidatos->Agregar($candi);
				header('location: ?controller=administrador&accion=home');
			}else
			{
				$part = new Model_Partidos();

				$part->IdPartido = $_REQUEST['IdPartido'];
				$part->NombrePartido = $_REQUEST['NombrePartido'];
				$part->Imagen = $_REQUEST['Imagen'];

				$part->IdPartido >0 ? $this->partidos->Actualizar($part) : $this->partidos->Agregar($part);
				header('location: ?controller=controller&accion=home');
			}
		}
		function Eliminar()
		{
			if (isset($_REQUEST['IdCandidato'])){
				$candi = new Model_Candidatos();

				$candi->IdCandidato = $_REQUEST['IdCandidato'];
				$this->Candidatos->Eliminar($candi);
				header('location: ?controller=controller&accion=home');
			}else 
			{
				$part = new Model_Partidos();

				$part->IdPartido = $_REQUEST['IdPartido'];
				$this->partidos->Eliminar($part);
			}
		}
	}