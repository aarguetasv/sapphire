<?php
	
	include_once('view/meta.php');
	include_once('autoload.php');
	
	$controller = new Controller_sapphire();

	if(!isset($_REQUEST['controller']))
	{
		$controller->Index();
	}
	else
	{
		$control = $_REQUEST['controller'];
		$accion = isset($_REQUEST['accion']) ? $_REQUEST['accion'] : 'Index';
		call_user_func(array($controller,$accion));
	}
 
?>
