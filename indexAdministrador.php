<?php

	include_once('view/meta.php');
	require 'autoload.php';

	$controller = new Controller_administrador();

	if(!isset($_REQUEST['controller']))
	{
		$controller->Index();
	}
	else
	{
		$control = $_REQUEST['controller'];
		$accion = isset($_REQUEST['accion']) ? $_REQUEST['accion'] : 'Index';
		call_user_func(array($controller,$accion));
	}
